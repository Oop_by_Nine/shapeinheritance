/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.shapinheritance;

/**
 *
 * @author Nine
 */
public class Rectangle extends Shape{

    private double h;
    private double w;
    final double eq = h * w;

    public Rectangle(double h, double w) {
        this.h = h;
        this.w = w;
        System.out.println("Rectangle created");
    }

    public double squareArea() {
        return h * w;
    }

    public double getR() {
        return h;

    }

    public double getR1() {
        return w;
    }

    @Override
    public void CalArea() {
        super.CalArea();
        System.out.println("Rectangle:" + getR() + ", " + getR1());
    }

    @Override
    public void Result() {
        super.Result();
        System.out.println("result: " + squareArea());
    }
}
