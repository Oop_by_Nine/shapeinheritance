/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.shapinheritance;

/**
 *
 * @author Nine
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.CalArea();
        System.out.println("Area of Circle1(r = " + circle1.getR() + " )is " + circle1.circleArea());
        circle1.Result();
        System.out.println("----------------------------");
          Triangle triangle1 = new Triangle(3, 4);
          triangle1.CalArea();
            System.out.println("Area of Triangle(b,h) = " + triangle1.getR() + "," + triangle1.getR1() + " ) is " + triangle1.triangleArea());
          triangle1.Result();
          System.out.println("----------------------------");
          Square square1 = new Square(2);
          square1.CalArea();
            System.out.println("Area of Square(s = " + square1.getR() + " ) is " + square1.squareArea());
          square1.Result();
          System.out.println("----------------------------");
         Rectangle rectangle1 = new Rectangle(3, 4);
         rectangle1.CalArea();
            System.out.println("Area of rectangle1((h,w) = " + rectangle1.getR() + "," + rectangle1.getR1() + " ) is " + rectangle1.squareArea());
         rectangle1.Result();
         
         System.out.println("----------------------------");
         
         Shape[] shape = {circle1,triangle1,square1,rectangle1};
        for(int i=0; i< shape.length; i++){
            shape[i].CalArea();
            shape[i].Result();
         
        }
    }
}
