/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.shapinheritance;

/**
 *
 * @author Nine
 */
public class Triangle extends Shape {

    double b;
    double h;
    final double tr = b * h / 2;

    public Triangle(double b, double h) {
        this.b = b;
        this.h = h;
        System.out.println("Triangle created");
    }

    public double triangleArea() {
        return b * h / 2;
    }

    public double getR() {
        return b;
    }

    public double getR1() {
        return h;
    }

    @Override
    public void CalArea() {
        super.CalArea();
        System.out.println("Triangle:" + getR() + ", " + getR1());
    }

    @Override
    public void Result() {
        super.Result();
        System.out.println("result: " + triangleArea());
    }
}
