/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.shapinheritance;

/**
 *
 * @author Nine
 */
public class Square extends Shape {

    double s;
    double fo = s * s;

    public Square(double s) {
        this.s = s;
        System.out.println("Square created");
    }

    public double squareArea() {
        return s * s;
    }

    public double getR() {
        return s;
    }

    @Override
    public void CalArea() {
        super.CalArea();
        System.out.println("Square:" + getR());
    }

    @Override
    public void Result() {
        super.Result();
        System.out.println("result: " + squareArea());
    }
}
