/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.shapinheritance;

/**
 *
 * @author Nine
 */
public class Circle extends Shape {

    private double r;
    public static final double pi = 22.0 / 7;

    public Circle(double r) {
        this.r = r;
        System.out.println("Circle created");
    }

    public double circleArea() {
        return pi * r * r;
    }

    public double getR() {
        return r;
    }

    @Override
    public void CalArea() {
        super.CalArea();
        System.out.println("Circle:" + getR());
    }

    @Override
    public void Result() {
        super.Result();
        System.out.println("result: " + circleArea());

    }
}
